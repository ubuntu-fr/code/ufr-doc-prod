# acl.auth.php
# <?php exit()?>
# Don't modify the lines above
#
# Access Control Lists
#
# Editing this file by hand shouldn't be necessary. Use the ACL
# Manager interface instead.
#
# If your auth backend allows special char like spaces in groups
# or user names you need to urlencode them (only chars <128, leave
# UTF-8 multibyte chars as is)
#
# none   0
# read   1
# edit   2 (édition des pages)
# create 4 (creation des pages)
# upload 8 (upload de fichiers)
# delete 16 (suppression de fichiers)
# FIXME Il est impossible de bloquer la création de namespaces sans bloquer
# la création de pages.
# %GROUP% correspond à n'importe quel groupe de l'utilisateur connecté
# %USER%  correspond à l'identifiant de l'utilisateur connecté


# Accès général non connecté
*                            @ALL               2

accueil                      @ALL               1
codedeconduite               @ALL               1
conduiteirc                  @ALL               1
debutant                     @ALL               1
installation                 @ALL               1

evenements:*                 @ALL               1
groupes:*                    @ALL               1
groupetraducteur:*           @ALL               1
playground:*                 @ALL               16
projets:*                    @ALL               1
traductions:*                @ALL               1
ubuntu-l10n-fr:*             @ALL               1
wiki:*                       @ALL               1

# Accès membres connectés
*                            %GROUP%            16
evenements:*                 %GROUP%            16
groupes:*                    %GROUP%            16
groupetraducteur:*           %GROUP%            16
playground:*                 %GROUP%            16
projets:*                    %GROUP%            16
traductions:*                %GROUP%            16
ubuntu-l10n-fr:*             %GROUP%            16
wiki:*                       %GROUP%            2

# Accès spécifiques espace utilisateurs
utilisateurs:*               @ALL               1
utilisateur:*                @ALL               1
utilisateurs:%USER%          %USER%             16
utilisateurs:%USER%:*        %USER%             16
utilisateurs:*               %GROUP%            2
utilisateurs:*:*             %GROUP%            2

# Ajout pour eviter le hook bootstrap3
sidebar                      @ALL               1
rightsidebar                 @ALL               1
navbar                       @ALL               1
pageheader                   @ALL               1
pagefooter                   @ALL               1
footer                       @ALL               1
cookie:banner                @ALL               1
cookie:policy                @ALL               1
help                         @ALL               1
header                       @ALL               1
topheader                    @ALL               1

