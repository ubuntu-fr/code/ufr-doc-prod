if(toolbar){

  toolbar[toolbar.length] = {"type":"format",
                             "title":"Modifier un fichier",
                             "icon":"editer_fichier.png",
                             "key":"",
                             "open":"[[:tutoriel:comment_modifier_un_fichier|",
                             "close":"]]",
                             "sample":"modifiez le fichier"};
                             
  toolbar[toolbar.length] = {"type":"format",
                             "title":"Installer un paquet",
                             "icon":"install_paquet.png",
                             "key":"",
                             "open":"[[:tutoriel:comment_installer_un_paquet|",
                             "close":"]]",
                             "sample":"installez le paquet"};

  toolbar[toolbar.length] = {"type":"format",
                             "title":"Apt Url",
                             "icon":"apt_url.png",
                             "key":"",
                             "open":"**[[apt>",
                             "close":"]]**",
                             "sample":"paquet1,paquet2,paquet3|paquet1 paquet2 paquet3"};



}
