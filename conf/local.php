<?php
/*
  Les paramètres ci dessous sont ceux du serveur de production. Les paramètres
  propres au serveur de développement (ou à vos propres configurations serveurs)
  doivent être redéfinis dans local.protected.php.
 */

$conf['lang']        = 'fr';
$conf['start']       = 'accueil';
$conf['title']       = 'Wiki ubuntu-fr';
$conf['tagline']       = 'La Documentation francophone';

$conf['recent']      = 40;
$conf['typography']  = 0;
$conf['dformat']     = 'Le %d/%m/%Y, %H:%M';
$conf['toptoclevel'] = 2;
$conf['indexdelay']  = 0;

//Gestion du thème

$conf['template']                                 = 'ubuntu';
/*
$conf['tpl']['bootstrap3']['bootstrapTheme']      = 'bootswatch';
$conf['tpl']['bootstrap3']['bootswatchTheme']     = 'united';
$conf['tpl']['bootstrap3']['showThemeSwitcher']   = 0;
$conf['tpl']['bootstrap3']['showCookieLawBanner'] = 1;
$conf['tpl']['bootstrap3']['fixedTopNavbar'] ='1';
$conf['tpl']['bootstrap3']['pageToolsAnimation'] ='0';
*/

$conf['is_dev']      = 0;

// gestion des connexions utilisateurs.
$conf['useacl']      = 1;
$conf['authtype']    = 'mysql';
$conf['passcrypt']   = 'sha1';
$conf['defaultgroup']= 'Members';
$conf['superuser']   = '@adminwiki';
$conf['manager']     = '@expert wiki';

// Configuration principale
$conf['disableactions']='register,profile,resendpwd,check,debug';
$conf['allowdebug']  = 0;
$conf['updatecheck'] = 0;
$conf['userewrite']  = 1;
$conf['useslash']    = 1;
$conf['locktime']    = 10*60;
$conf['fetchsize']   = 2*1024*1024;
$conf['spellchecker']= 0;
$conf['subscribers'] = 1;
$conf['rss_update']  = 20*60;
$conf['recent_days'] = 400; // Reduire ce nombre si la consomation mémoire est trop importante
$conf['signature'] = '--- //[[:utilisateurs:@USER@|@USER@]] @DATE@//';
// Suivi des modifications
$conf['notify'] = 'ubuntu_wiki_modifs-fr@lists.ubuntu-eu.org';
$conf['mailprefix'] = 'UFrDoc';
$conf['license'] = 'cc-by-sa';
$conf['mailfrom'] = 'ufr_doc@ubuntu-fr.org';
$conf['htmlmail']    = 0;

// Plugins
$conf['plugin']['tag']['pagelist_flags'] = 'table,header,nodate,nouser';
$conf['plugin']['tag']['namespace']      =':';
$conf['readdircache']                    = 120;     // 2 minutes de cache sur les acces disques
$conf['plugin']['captcha']['mode']       = 'js';
