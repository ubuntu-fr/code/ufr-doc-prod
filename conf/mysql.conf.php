<?php
/*
 * This is an example configuration for the mysql auth module.
 *
 * This SQL statements are optimized for following table structure.
 * If you use a different one you have to change them accordingly.
 * See comments of every statement for details.
 *
 * TABLE users
 *     uid   login   pass   firstname   lastname   email
 *
 * TABLE groups
 *     gid   name
 *
 * TABLE usergroup
 *     uid   gid
 *
 */

/* Options to configure database access. You need to set up this
 * options carefully, otherwise you won't be able to access you
 * database.
 */
//$conf['plugin']['authmysql']['server']   = '';
//$conf['plugin']['authmysql']['user']     = '';
//$conf['plugin']['authmysql']['password'] = '';
//$conf['plugin']['authmysql']['database'] = '';
//$conf['plugin']['authmysql']['prefix']   = '';

/* variable to change the prefix of the tables
 */
$db_prefix = $conf['plugin']['authmysql']['prefix'];

/* This option enables debug messages in the mysql module. It is
 * mostly usefull for system admins.
 */
$conf['plugin']['authmysql']['debug'] = 0;

/* Normally password encryption is done by DokuWiki (recommended) but for
 * some reasons it might be usefull to let the database do the encryption.
 * Set 'forwardClearPass' to '1' and the cleartext password is forwarded to
 * the database, otherwise the encrypted one.
 */
$conf['plugin']['authmysql']['forwardClearPass'] = 0;

/* Multiple table operations will be protected by locks. This array tolds
 * the module which tables to lock. If you use any aliases for table names
 * these array must also contain these aliases. Any unamed alias will cause
 * a warning during operation. See the example below.
 */
$conf['plugin']['authmysql']['TablesToLock']= array("${db_prefix}users", "${db_prefix}users AS u", "${db_prefix}groups", "${db_prefix}groups AS g");

/***********************************************************************/
/*       Basic SQL statements for user authentication (required)       */
/***********************************************************************/

$conf['plugin']['authmysql']['checkPass']   = "SELECT  u.password AS pass
                                         FROM    ${db_prefix}users AS u, ${db_prefix}groups AS g
                                         WHERE   u.group_id  = g.g_id
                                         AND     u.username  = '%{user}'
                                         AND     g.g_title  != 'Guest'";
    
$conf['plugin']['authmysql']['getUserInfo'] = "SELECT  password AS pass,
                                                 realname AS name,
                                                 email AS mail,
                                                 id,
                                                 g_title as `group`
                                         FROM    ${db_prefix}users AS u, 
                                                 ${db_prefix}groups AS g
                                         WHERE   u.group_id = g.g_id
                                         AND     u.username = '%{user}'";

$conf['plugin']['authmysql']['getGroups']   = "SELECT  g.g_title as `group`
                                         FROM    ${db_prefix}users AS u, 
                                                 ${db_prefix}groups AS g
                                         WHERE   u.group_id = g.g_id
                                         AND     u.username = '%{user}'";

/***********************************************************************/
/*      Additional minimum SQL statements to use the user manager      */
/***********************************************************************/

$conf['plugin']['authmysql']['getUsers']    = "SELECT DISTINCT u.username AS user
                                         FROM    ${db_prefix}users AS u, 
                                                 ${db_prefix}groups AS g
                                         WHERE   u.group_id = g.g_id";
$conf['plugin']['authmysql']['FilterLogin'] = "u.username LIKE '%{user}'";
$conf['plugin']['authmysql']['FilterName']  = "u.realname LIKE '%{name}'";
$conf['plugin']['authmysql']['FilterEmail'] = "u.email    LIKE '%{email}'";
$conf['plugin']['authmysql']['FilterGroup'] = "g.g_title  LIKE '%{group}'";
$conf['plugin']['authmysql']['SortOrder']   = "ORDER BY u.username";

/***********************************************************************/
/*   Additional SQL statements to add new users with the user manager  */
/***********************************************************************/


$conf['plugin']['authmysql']['addUser']     = "INSERT INTO ${db_prefix}users
                                                (username,   password, email,      realname,  language,   registered, registration_ip, last_visit)
                                         VALUES ('%{user}', '%{pass}', '%{email}', '%{name}', 'Francais', %{time},    '%{ip}',         %{time})";

$conf['plugin']['authmysql']['addGroup']    = "INSERT INTO ${db_prefix}groups (g_title) VALUES ('%{group}')";

$conf['plugin']['authmysql']['addUserGroup']= "UPDATE ${db_prefix}users
                                         SET   group_id=%{gid}
                                         WHERE id='%{uid}'";

$conf['plugin']['authmysql']['delGroup']    = "DELETE FROM ${db_prefix}groups WHERE g_id='%{gid}'";

$conf['plugin']['authmysql']['getUserID']   = "SELECT id FROM ${db_prefix}users WHERE username='%{user}'";

/***********************************************************************/
/*   Additional SQL statements to delete users with the user manager   */
/***********************************************************************/

/* This statement should remove a user fom the database.
 * Following patterns will be replaced:
 *   %{user}    user's login name
 *   %{uid}     id of a user dataset
 */
//$conf['plugin']['authmysql']['delUser']     = "DELETE FROM users
//                                         WHERE uid='%{uid}'";

/* This statement should remove all connections from a user to any group
 * (a user quits membership of all groups).
 * Following patterns will be replaced:
 *   %{uid}     id of a user dataset
 */
//$conf['plugin']['authmysql']['delUserRefs'] = "DELETE FROM usergroup
//                                         WHERE uid='%{uid}'";

/***********************************************************************/
/*   Additional SQL statements to modify users with the user manager   */
/***********************************************************************/

$conf['plugin']['authmysql']['updateUser']  = "UPDATE ${db_prefix}users SET";
$conf['plugin']['authmysql']['UpdateLogin'] = "username='%{user}'";
$conf['plugin']['authmysql']['UpdatePass']  = "password='%{pass}'";
$conf['plugin']['authmysql']['UpdateEmail'] = "email='%{email}'";
$conf['plugin']['authmysql']['UpdateName']  = "realname='%{name}'";
$conf['plugin']['authmysql']['UpdateTarget']= "WHERE id=%{uid}";

$conf['plugin']['authmysql']['delUserGroup']= "UPDATE ${db_prefix}users SET g_id=4 WHERE id=%{uid}";

$conf['plugin']['authmysql']['getGroupID']  = "SELECT g_id AS id FROM ${db_prefix}groups WHERE g_title='%{group}'";


