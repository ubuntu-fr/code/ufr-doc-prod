<?php
$license['cc-by-sa'] = array(
    'name' => 'CC Paternité-Partage des Conditions Initiales à l\'Identique 3.0 Unported',
    'url'  => 'http://creativecommons.org/licenses/by-sa/3.0/deed.fr',
);
