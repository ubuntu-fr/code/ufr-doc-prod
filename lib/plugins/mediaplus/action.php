<?php
/**
 * Action Plugin Prototype
 *
 * @license    GPL 2 (http://www.gnu.org/licenses/gpl.html)
 * @author     Philippe Cortez <dokuwiki@yoboy.fr>
 */
// must be run within Dokuwiki
if(!defined('DOKU_INC')) die();
if(!defined('DOKU_PLUGIN')) define('DOKU_PLUGIN',DOKU_INC.'lib/plugins/');
require_once(DOKU_PLUGIN.'action.php');

/**
 * All DokuWiki plugins to interfere with the event system
 * need to inherit from this class
 */
class action_plugin_mediaplus extends Dokuwiki_Action_Plugin {

    /**
     * Registers a callback function for a given event
     */
    function register(&$controller) {
        $controller->register_hook('MEDIAMANAGER_CONTENT_OUTPUT', 'BEFORE', $this, handle_mediaplus, array());
    }

    function handle_mediaplus(&$event, $param) {
	    global $IMG;
	    global $AUTH;
    	global $INUSE;
    	global $NS;
    	global $JUMPTO;

	    $data = $event->data;
        $do = $data['do'];
        if($do == 'filesinuse'){
            media_filesinuse($INUSE,$IMG);
        }elseif($do == 'filelist'){
            $this->_mediaplus_filelist($NS,$AUTH,$JUMPTO);
        }elseif($do == 'searchlist'){
            media_searchlist($_REQUEST['q'],$NS,$AUTH);
        }else{
            msg('Unknown action '.hsc($do),-1);
        }
	    $event->preventDefault();
    }

    /**
     * List all files in a given Media namespace
     */
    function _mediaplus_filelist($ns,$auth=null,$jump='', $sort=false){
        global $conf;
        global $lang;
        $ns = cleanID($ns);
        $bypages = $this->getConf('bypages');
    
        if($_REQUEST['page']) $page = $_REQUEST['page'];
        else $page = 1;
        
        // check auth our self if not given (needed for ajax calls)
        if(is_null($auth)) $auth = auth_quickaclcheck("$ns:*");

        echo '<h1 id="media__ns">:'.hsc($ns).'</h1>'.NL;

        if($auth < AUTH_READ){
            // FIXME: print permission warning here instead?
            echo '<div class="nothing">'.$lang['nothingfound'].'</div>'.NL;
        }else{
            media_uploadform($ns, $auth);

            $dir = utf8_encodeFN(str_replace(':','/',$ns));
            $data = array();
            search($data,$conf['mediadir'],'search_media',
                    array('showmsg'=>true,'depth'=>1),$dir,1,$sort);

            if(!count($data)){
                echo '<div class="nothing">'.$lang['nothingfound'].'</div>'.NL;
            }else {
                if ($jump != '') {
                    // go to the page where the item is
                    $idkey = array_search($jump, $data);
                    $page = (int)($idkey/$bypages +1);
                    $jump = (int)($idkey%$bypages);
                }
                $pagemax = (int)(count($data)/$bypages +1);
                $link = DOKU_BASE.'lib/exe/mediamanager.php?ns='.$ns.'&page=';
                if ($page > $pagemax) $page = $pagemax;
                $data = array_slice($data, ($page-1)*$bypages, $bypages);
                
                if ($pagemax > 1) echo $this->_mediaplus_pagination($page, $pagemax, $link);
                foreach($data as $item){
                    $this->_mediaplus_printfile($item,$auth,$jump);
                }
                if ($pagemax > 1) echo $this->_mediaplus_pagination($page, $pagemax, $link);
            }
        }
        media_searchform($ns);
    }


    function _mediaplus_pagination($page, $pagemax, $link) {
        $paginate = '<div class="pagination">'.NL;
        if ($page > 1) $paginate .= '<a href="'.$link.($page-1).'" class="pagination">< '.$this->getLang('previous').'</a> '.NL;
        if ($page == 1 ) $paginate .= '<b>1</b> '.NL;
        else $paginate .= '<a href="'.$link.'1" class="pagination">1</a> '.NL;
        $min = ($page-4 > 1 ? $page -4 : 2);
        $max = ($page + 4 < $pagemax ? $page +4 : $pagemax-1);
        if($min > 2) $paginate .= "… ";
        for($i=$min; $i<=$max; $i++) {
            if($i == $page) $paginate .= '<b>'.$i.'</b> '.NL;
            else $paginate .= '<a href="'.$link.$i.'" class="pagination">'.$i.'</a> '.NL;
        }
        if ($max < $pagemax-1) $paginate .= '… '.NL;
        if ($page == $pagemax) $paginate .= '<b>'.$pagemax.'</b> '.NL;
        else $paginate .= '<a href="'.$link.$pagemax.'">'.$pagemax.'</a> '.NL;
        if ($page < $pagemax) $paginate .= '<a href="'.$link.($page+1).'">'.$this->getLang('next').' ></a>'.NL;
        $paginate .= ' <span class="itemsinfo">('.$this->getConf('bypages').' '.$this->getLang('items per pages').')</span>'.NL.'</div>'.NL;
        return $paginate;
    }

    /**
     * Formats and prints one file in the list
     */
    function _mediaplus_printfile($item,$auth,$jump,$display_namespace=false){
        global $lang;
        global $conf;

        // Prepare zebra coloring
        // I always wanted to use this variable name :-D
        static $twibble = 1;
        $twibble *= -1;
        $zebra = ($twibble == -1) ? 'odd' : 'even';

        // Automatically jump to recent action
        if($jump == $item['id']) {
            $jump = ' id="scroll__here" ';
        }else{
            $jump = '';
        }

        // Prepare fileicons
        list($ext,$mime,$dl) = mimetype($item['file'],false);
        $class = preg_replace('/[^_\-a-z0-9]+/i','_',$ext);
        $class = 'select mediafile mf_'.$class;

        // Prepare filename
        $file = utf8_decodeFN($item['file']);

        // Prepare info
        $info = '';
        if($item['isimg']){
            $info .= (int) $item['meta']->getField('File.Width');
            $info .= '&#215;';
            $info .= (int) $item['meta']->getField('File.Height');
            $info .= ' ';
        }
        $info .= '<i>'.dformat($item['mtime']).'</i>';
        $info .= ' ';
        $info .= filesize_h($item['size']);

        // output
        echo '<div class="'.$zebra.'"'.$jump.' title="'.hsc($item['id']).'">'.NL;
        if (!$display_namespace) {
            echo '<a name="h_:'.$item['id'].'" class="'.$class.'">'.hsc($file).'</a> ';
        } else {
            echo '<a name="h_:'.$item['id'].'" class="'.$class.'">'.hsc($item['id']).'</a><br/>';
        }
        echo '<span class="info">('.$info.')</span>'.NL;

        // view button
        $link = ml($item['id'],'',true);
        echo ' <a href="'.$link.'" target="_blank"><img src="'.DOKU_BASE.'lib/images/magnifier.png" '.
            'alt="'.$lang['mediaview'].'" title="'.$lang['mediaview'].'" class="btn" /></a>';

        // mediamanager button
      /*  $link = wl('',array('do'=>'media','image'=>$item['id'],'ns'=>getNS($item['id'])));
        echo ' <a href="'.$link.'" target="_blank"><img src="'.DOKU_BASE.'lib/images/mediamanager.png" '.
            'alt="'.$lang['btn_media'].'" title="'.$lang['btn_media'].'" class="btn" /></a>';*/

        // delete button
        if($item['writable'] && $auth >= AUTH_DELETE){
            $link = DOKU_BASE.'lib/exe/mediamanager.php?delete='.rawurlencode($item['id']).
                '&amp;sectok='.getSecurityToken();
            echo ' <a href="'.$link.'" class="btn_media_delete" title="'.$item['id'].'">'.
                '<img src="'.DOKU_BASE.'lib/images/trash.png" alt="'.$lang['btn_delete'].'" '.
                'title="'.$lang['btn_delete'].'" class="btn" /></a>';
        }

        echo '<div class="example" id="ex_'.str_replace(':','_',$item['id']).'">';
        echo $lang['mediausage'].' <code>{{:'.$item['id'].'}}</code>';
        echo '</div>';
        if($item['isimg']) media_printimgdetail($item);
        echo '<div class="clearer"></div>'.NL;
        echo '</div>'.NL;
    }

}
