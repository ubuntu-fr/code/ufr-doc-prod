<?php
/**
 * French language file
 *
 * @license    GPL 2 (http://www.gnu.org/licenses/gpl.html)
 */

// custom language strings for the plugin
$lang['previous'] = 'Précédent';
$lang['next'] = 'Suivant';
$lang['items per pages'] = 'éléments par pages';
