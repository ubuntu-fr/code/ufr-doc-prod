<?php
/**
 * English language file
 *
 * @license    GPL 2 (http://www.gnu.org/licenses/gpl.html)
 */

// custom language strings for the plugin
$lang['previous'] = 'Previous';
$lang['next'] = 'Next';
$lang['items per pages'] = 'items per pages';
