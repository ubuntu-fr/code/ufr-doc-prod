<?php
/**
 * Default settings for the version plugin
 *
 * @author YoBoY <yoboy@ubuntu-fr.org>
 */

$conf['exclude']    = 'portail';
$conf['alertNoTags'] = 'Cette page n\'a pas encore d\'<a href="/wiki/tags">étiquettes</a>.';
$conf['excludeID'] = array(
                        '/^(accueil|Accueil)$/',
                        '/(warty|hoary|breezy|dapper|edgy|feisty|gutsy|hardy|intrepid|jaunty|karmic|lucid|maverick|natty|oneiric|precise|quantal|raring|saucy|trusty|utopic)/',
                        '/(4\.10|5\.04|5\.10|6\.06|6\.10|7\.04|7\.10|8\.04|8\.10|9\.04|9\.10|10\.04|10\.10|11\.04|11\.10|12\.04|12\.10|13\.04|13\.10|14\.04|14\.10)/',
                );
$conf['excludeNS'] = array(
                        'evenements',
                        'groupes',
                        'playground',
                        'projets',
                        'ubuntu-l10n-fr',
                        'utilisateurs',
                        'wiki',
                );
$conf['wrongTag'] = 'Une des étiquettes de cette page n\'a pas d\'autres pages associées.';



$conf['notes'] = array(

    'warty hoary breezy dapper edgy feisty gutsy hardy intrepid jaunty karmic lucid maverick natty oneiric precise quantal raring saucy utopic vivid yakkety zetsy artful -trusty -xenial -bionic'
        => 'Selon les <a href="/tags">tags</a> présents sur cette page, celle-ci n\'a pas été vérifiée pour une des <a href="/versions">versions LTS supportées d\'Ubuntu</a>.',
//    'precise -quantal -raring -saucy -trusty -utopic'
//        => 'Selon les <a href="/tags">tags</a> présents sur cette page, les informations qu\'elle contient n\'ont pas été vérifiées depuis <a href="/precise">Ubuntu 12.04 LTS.</a>',
//    'lucid -precise -quantal -raring -saucy -trusty -utopic'
//        => 'Selon les <a href="/tags">tags</a> présents sur cette page, les informations qu\'elle contient  n\'ont pas été vérifiées depuis <a href="/lucid">Ubuntu 10.04 LTS.</a>',
    'trusty -xenial -bionic'
        => 'Selon les <a href="/tags">tags</a> présents sur cette page, les informations qu\'elle contient  n\'ont pas été vérifiées pour les dernières versions LTS depuis <a href="/trusty">Ubuntu 14.04 LTS.</a>',
//    'xenial -bionic'
//        => 'Selon les <a href="/tags">tags</a> présents sur cette page, les informations qu\'elle contient  n\'ont pas été vérifiées pour les dernières versions LTS depuis <a href="/trusty">Ubuntu 16.04 LTS.</a>',
    'vetuste vétuste'
        => 'Cette page est considérée comme <a href="/vetuste">vétuste</a> et ne contient plus d\'informations utiles.',
    'BROUILLON brouillon'
        => 'Cette page est en cours de rédaction.',

                           );
$conf['prependText'] = "";
$conf['appendText'] = "<a href=\"/wiki/participer_wiki\">Apportez votre aide…</a>".DOKU_LF;

