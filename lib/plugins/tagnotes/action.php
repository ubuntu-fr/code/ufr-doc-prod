<?php
/**
 * DokuWiki Plugin version (Action Component)
 *
 * @license GPL 2 http://www.gnu.org/licenses/gpl-2.0.html
 * @author  YoBoY <yoboy@ubuntu-fr.org>
 */

// must be run within Dokuwiki
if (!defined('DOKU_INC')) die();

if (!defined('DOKU_LF')) define('DOKU_LF', "\n");
if (!defined('DOKU_TAB')) define('DOKU_TAB', "\t");
if (!defined('DOKU_PLUGIN')) define('DOKU_PLUGIN',DOKU_INC.'lib/plugins/');

require_once(DOKU_PLUGIN.'action.php');

class action_plugin_tagnotes extends DokuWiki_Action_Plugin {

    /**
     * return some info
     */
    function getInfo() {
        return array(
                'author' => 'YoBoY',
                'email'  => 'YoBoY',
                'date'   => '2012-03-20',
                'name'   => 'Tag Notes Plugin',
                'desc'   => 'Add top notes to a page with or without some tags.',
                'url'    => 'http://launchpad.net/ubuntu-fr-doc',
                );
    }


    function register(&$contr) {
       $contr->register_hook('TPL_CONTENT_DISPLAY', 'BEFORE', $this, '_handleTagNotes', array());
    }

    function _handleTagNotes(&$event, $param) {
        global $ID;
        global $ACT;
        global $INFO;
        if ($ACT == 'show') {
            if (!$my =& plugin_load('helper', 'tag')) return false;
            $tags = $my->_getSubjectMetadata($ID);
            $notes = "";
            // no notes if page doesn't exists
            if (!$INFO['exists']) return false;
            // excluded pages and NS
            foreach ($this->getConf('excludeNS') as $pageNS) {
                $nstested = $INFO['namespace'];
                while ($nstested) {
                    if($pageNS == $nstested) return false;
                    $nstested = getNS($nstested);
                }
            }
            foreach ($this->getConf('excludeID') as $pageID) {
                if(preg_match($pageID, $ID)) return false;
            }

            // page with excluding tags (no notes when there is these tags)
            $excludeTags = $my->_parseTagList($this->getConf('exclude'));
            if ($my->_checkPageTags($tags, $excludeTags)) return false;

            // the page have no tags
            if ((empty($tags) || !$tags[0]) && $this->getConf('alertNoTags') != "") {
                $tags = array();
                $notes = $this->getConf('alertNoTags')."<br />".DOKU_LF;
            }

            // Some tags have no other page associated
            if ($this->getConf('wrongTag') != "") {
                $occurrences = $my->tagOccurrences($tags);
                foreach($tags as $tag ) {
                    if (!isset($occurrences[$tag]) || $occurrences[$tag] == 1) {
                        $notes .= $this->getConf('wrongTag')."<br />".DOKU_LF;
                        break;
                    }
                }
            }

            // the other pages not excluded until this point
            $tagsNotes = $this->getConf('notes');
            foreach ($tagsNotes as $tagCond => $tagNote) {
                $tagConds = $my->_parseTagList($tagCond);
                if($my->_checkPageTags($tags, $tagConds)) {
                    $notes .= $tagNote."<br />".DOKU_LF;
                }
            }
            if ($notes == '') return false;
            $notes = '<div class="notetag">'.$this->getConf('prependText').$notes.$this->getConf('appendText').'</div>'.DOKU_LF;
            $event->data = $notes . $event->data;
        }
    }

}

// vim:ts=4:sw=4:et:
