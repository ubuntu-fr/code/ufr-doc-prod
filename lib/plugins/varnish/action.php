<?php
/**
 * DokuWiki Plugin varnish (Action Component)
 *
 * @license GPL 2 http://www.gnu.org/licenses/gpl-2.0.html
 * @author  YoBoY <yoboy@ubuntu-fr.org>
 */

// must be run within Dokuwiki
if (!defined('DOKU_INC')) die();

if (!defined('DOKU_LF')) define('DOKU_LF', "\n");
if (!defined('DOKU_TAB')) define('DOKU_TAB', "\t");
if (!defined('DOKU_PLUGIN')) define('DOKU_PLUGIN',DOKU_INC.'lib/plugins/');

require_once DOKU_PLUGIN.'action.php';

class action_plugin_varnish extends DokuWiki_Action_Plugin {
// FIXME Use the this->getConf() method in this plugin.

    public function register(Doku_Event_Handler &$controller) {

       $controller->register_hook('IO_WIKIPAGE_WRITE', 'AFTER', $this, '_varnish_purge');
       $controller->register_hook('AUTH_LOGIN_CHECK', 'AFTER', $this, '_varnish_cookie');
   
    }

    /**
     * Delete an entry from varnish cache
     * 
     * Delete an entry from varnish cache by connecting on varnish socket 
     * and sending a "PURGE /url" request
     *
     */ 
    public function _varnish_purge(Doku_Event &$event, $param) {
        global $conf;
        global $ID;
        $fp = fsockopen($conf['varnish_host'], $conf['varnish_port'], $errno, $errstr, 3);
	$id_clean = str_replace(':','/',$ID);
        if (!$fp) {
            // FIXME Use dokuwiki error handler
            echo "$errstr ($errno)<br />\n";
        } else {
            $out = "PURGE /".$id_clean." HTTP/1.0\r\n";
            $out .= "Host: ".$_SERVER['HTTP_HOST']."\r\n";
            $out .= "Connection: Close\r\n\r\n";
            fwrite($fp, $out);
            if ($conf['varnish_debug']) {
                while (!feof($fp)) {
                    echo fgets($fp, 128);
                }
            }
            fclose($fp);
        }
    }

    /**
     * Set cookie for anonymous users
     * 
     * Don't allow dynamic data like breadcrumbs or youarehere
     * 
     */ 
    public function _varnish_cookie(Doku_Event &$event, $param) {
        global $conf;
        if (!isset($_SERVER['REMOTE_USER'])) {
            if (version_compare(PHP_VERSION, '5.2.0', '>')) {
                setcookie('VARNISHAUTH','',time()-600000,DOKU_REL,'',($conf['securecookie'] && is_ssl()),true);
            }else{
                setcookie('VARNISHAUTH','',time()-600000,DOKU_REL,'',($conf['securecookie'] && is_ssl()));
            }
            // We can't use also breadcrumbs or youarehere and have to force the conf to 0
            $conf['breadcrumbs'] = 0;
            $conf['youarehere'] = 0;
        } else {
            if (version_compare(PHP_VERSION, '5.2.0', '>')) {
                setcookie('VARNISHAUTH','1',0,DOKU_REL,'',($conf['securecookie'] && is_ssl()),true);
            }else{
                setcookie('VARNISHAUTH','1',0,DOKU_REL,'',($conf['securecookie'] && is_ssl()));
            }
            // breadcrumbs and youarehere not changed
        }
    }
}

// vim:ts=4:sw=4:et:
