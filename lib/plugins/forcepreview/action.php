<?php
/**
 * Force Preview Action Plugin: Force users to hit preview before saving changes to a wiki page
 * 
 * @author     Pascal Bihler <bihler@cs.uni-bonn.de>
 */
 
if(!defined('DOKU_INC')) die();
if(!defined('DOKU_PLUGIN')) define('DOKU_PLUGIN',DOKU_INC.'lib/plugins/');
require_once(DOKU_PLUGIN.'action.php');

class action_plugin_forcepreview extends DokuWiki_Action_Plugin {
 
  /**
   * return some info
   */
  function getInfo(){
    return array(
		 'author' => 'Pascal Bihler',
		 'email'  => 'bihler@cs.uni-bonn.de',
		 'date'   => '2012-02-09',
		 'name'   => 'Force preview',
		 'desc'   => 'Force users to hit preview before saving changes to a wiki page',
		 'url'    => 'http://wiki.splitbrain.org/plugin:forcepreview',
		 );
  }
 
  /*
   * Register its handlers with the dokuwiki's event controller
   */
  function register(&$controller) {
    	$controller->register_hook('TPL_ACT_RENDER', 'AFTER',  $this, '_controlSaveButton');
  }
 
  /**
   * Relocates the css-loading to own method
   */
  function _controlSaveButton(&$event, $param) {
    global $ACT;
    global $lang;
    
    $disable_save_button = '';
    if ($ACT == 'edit') {
        $disable_save_button = 'true';
    } elseif ($ACT == 'preview') {
        $disable_save_button = 'false';
    }
	if ($disable_save_button != '') {
	    print "<script type=\"text/javascript\">\n";
	    print "   var saveBtn = document.getElementById('edbtn__save');\n";
	    print "   saveBtn.disabled = $disable_save_button;\n";
	    if ($disable_save_button == 'true') {
	        print "   saveBtn.style.color = 'gray';\n";
	        print "   saveBtn.style.backgroundColor = '#EEEEEE';\n";
	        print "   saveBtn.value = '" . $lang['btn_save'] . " (Click \'" . $lang['btn_preview'] . "\' first)';\n";
	    }
	    print "</script>";
	}
  }
	
	
}