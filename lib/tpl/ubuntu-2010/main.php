<?php
    /**
     * Ubuntu-fr 2010 template
     *
     * @link   //ubuntu-fr.org/
     * @author ubuntu-fr community
     */

    // must be run from within DokuWiki
    if (!defined('DOKU_INC')) die();

    // Theme selection
    $domain = $_SERVER['HTTP_HOST'];
    $subdomain = tpl_getConf('subdomain');
    $domain_names = tpl_getConf('domain');
    $names = tpl_getConf('name');
    $comps = tpl_getConf('comp');
    $indexes = tpl_getConf('indexed');

    $tplname = 'ubuntu'; //default theme
    foreach ($domain_names as $tpl => $url) {
        $compdomain = $subdomain . $url;
        if ($domain == $compdomain) {
            $tplname = $tpl;
        }
    }
    $domain_name = $domain_names[$tplname];
    $distrib_name = $names[$tplname];
    $distrib_comp = $comps[$tplname];
    $is_ubuntu = $indexes[$tplname];
    if ($conf['is_dev']) {
        $is_ubuntu = 0;
    }

    if (!defined('UFR_STATIC_TPL')) define('UFR_STATIC_TPL', tpl_getConf('static_host') .'/theme2010');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?=$conf['lang']?>" lang="<?=$conf['lang']?>" dir="<?=$lang['direction']?>">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="Content-Script-Type" content="text/javascript"/>
    <meta http-equiv="Content-Style-Type" content="text/css"/>
    <meta http-equiv="Content-Language" content="fr"/>
    <title><?=$ID?> - <?=hsc($conf['title'])?></title>
    <meta name="Description" content="Documentation francophone pour la distribution Ubuntu" lang="fr" />
    <meta name="language" content="fr-FR" />

    <!--[if IE 8]>
    <meta http-equiv="X-UA-Compatible" content="IE=8" />
    <![endif]-->
    <? if (!$is_ubuntu): ?>
    <meta name="GOOGLEBOT" content="NOINDEX, NOFOLLOW" />
    <? endif ?>
    <?tpl_metaheaders()?>
    <link rel="shortcut icon" href="<?=UFR_STATIC_TPL?>/images/commun/<?=strtolower($distrib_name)?>/icone.png" type="image/x-icon" />
    <link rel="apple-touch-icon" href="<?=UFR_STATIC_TPL?>/images/commun/<?=strtolower($distrib_name)?>/touch-ico.png" />

    <link rel="stylesheet" media="screen" type="text/css" title="Design <?=$distrib_name?>-fr" href="<?=UFR_STATIC_TPL?>/css/doc.css" />
    <link rel="stylesheet" media="screen" type="text/css" title="Design <?=$distrib_name?>-fr" href="<?=UFR_STATIC_TPL?>/css/doc-<?=strtolower($distrib_name)?>.css" />
    <link rel="stylesheet" media="print" type="text/css" title="Design <?=$distrib_name?>-fr" href="<?=UFR_STATIC_TPL?>/css/doc-print.css" />

    <script type="text/javascript">
      var menu_hidden;
      var static_url = "<?=UFR_STATIC?>";
    </script>
    <script src="<?=UFR_STATIC_TPL?>/js/menu.js" type="text/javascript"></script>
    <script src="<?=UFR_STATIC_TPL?>/js/common.js" type="text/javascript"></script>
  </head>
  <body>

    <div id="accessibar">
      <a href="#main" tabindex="1">Contenu</a> | <a href="#qsearch__in" tabindex="2">Rechercher</a> | <a href="#navigation" tabindex="3">Menus</a>
    </div>

    <div id="page">

      <div id="header">

        <div id="logo">
          <h1><?=$distrib_name?>-fr</h1>
          <a href="/" title="Accueil de la documentation">Communauté francophone d'utilisateurs <?=$distrib_comp?><?=$distrib_name?></a>
        </div>

        <form action="//<?=$domain_name?>/recherche" accept-charset="utf-8" id="search" onsubmit="if (document.getElementById('qsearch__in').value == 'Recherche rapide....') {document.getElementById('qsearch__in').value = '';}">
          <fieldset>
            <label for="qsearch__in">Recherche : </label><input type="text" value="Recherche rapide...." id="qsearch__in" accesskey="f" name="sk_q" alt="[F]" size="34" tabindex="4" />
            <label for="tsearch_field">Chercher dans : </label><select name="sk_engine" tabindex="5" id="tsearch_field" title="Chercher dans">
              <option value="all">Site</option>
              <option selected="selected" value="doc">Documentation</option>
              <option value="forum">Forum</option>
              <option value="planet">Planet</option>
            </select>
            <input type="submit" value="ok" class="button" alt="Lancer la recherche" tabindex="5" />
          </fieldset>
        </form>
        <!--[if lte IE 7]><div class="clear"></div><![endif]-->

<?php if(isset($_SERVER['REMOTE_USER'])): ?>
        <div id="login">
<?php tpl_userinfo(); ?> / <?php tpl_button('login'); ?>
        </div>
<?php else: ?>
        <form action="" accept-charset="utf-8" id="login_top" method="post">
          <fieldset>
            <label for="u_field">Identifiant : </label><input type="text" value="Identifiant" name="u" id="u_field" size="9" /><label for="p_field">Mot de passe : </label><input type="password" value="Mot de passe" name="p" id="p_field" size="9" alt="Mot de passe" /><input type="submit" value="connexion" id="connect" /> / <a href="//forum.<?=$domain_name?>/register.php">inscription</a>
          </fieldset>
        </form>
<?php endif; ?>

      </div>

      <div id="navbar">
          <h2 id="pagetitle"><?tpl_link(wl($ID,''),$ID)?></h2>
      </div>

      <?php flush()?>

      <div id="main" class="dokuwiki">

        <div id="hidemenu" title="Masquer le menu"></div>

        <div id="pagerror"><?php html_msgarea()?></div>
        <?if($conf['breadcrumbs']){?><p><?tpl_breadcrumbs()?></p><?}?>
        <?tpl_content()?>
        <br style="clear:both;" />
        <div id="pageinfo">
            <!-- <?tpl_userinfo()?> | <?php echo $INFO['userinfo']['mail']. ' | '. $INFO['userinfo']['gprs']['0'] ?>-->
            <!-- ?tpl_pageinfo()? -->
            <br />
            Le contenu de ce wiki est sous licence : <a href="//creativecommons.org/licenses/by-sa/3.0/deed.fr" rel="license" target="_blank">CC BY-SA v3.0</a>
        </div>
      </div>

      <?php flush()?>

      <div id="navigation">

        <ul>
          <li class="menu"><a href="//www.<?=$domain_name?>" class="title" id="menu-accueil"><span>Accueil</span></a></li>
          <li class="menu" id="active">
            <a href="/"  class="title" id="menu-doc"><span>Documentation</span></a>
            <ul>
              <li id="navWiki" class="cat">
                <h2>Actions</h2>
                <ul>
                  <li><?php tpl_actionlink('index')?></li>
                  <li><?php tpl_actionlink('edit')?></li>
                  <li><?php tpl_actionlink('history')?></li>
                  <li><?php tpl_actionlink('revert')?></li>
                  <li><?php tpl_actionlink('recent')?></li>
                  <li><?php tpl_actionlink('backlink')?></li>
                  <li><?php tpl_actionlink('subscription')?></li>
                  <li><?php tpl_actionlink('media')?></li>
                  <li><?php tpl_actionlink('admin')?></li>
                </ul>
              </li>
              <li id="navDivers" class="cat">
                <h2>Divers</h2>
                <ul>
                  <li><?php tpl_pagelink(":wiki:participer_wiki", "Participer à la documentation") ?></li>
                  <li><?php tpl_pagelink(":documentation_hors_ligne", "Documentation hors ligne") ?></li>
                  <li><a href="//www.<?=$domain_name?>/telechargement" title="T&eacute;l&eacute;charger <?=$distrib_name?>">T&eacute;l&eacute;charger <?=$distrib_name?></a></li>
                </ul>
              </li>
            </ul>
          </li>
          <li class="menu"><a href="//forum.<?=$domain_name?>"  class="title" id="menu-forum"><span>Forum</span></a></li>
          <li class="menu"><a href="//planet.<?=$domain_name?>"  class="title" id="menu-planet"><span>Planet</span></a></li>
        </ul>

      </div>
      <div id="references-ufr">
        <ul id="legal-ufr">
          <li><a href="//www.<?=$domain_name?>/contacts">Contact</a></li>
        </ul>

        <ul id="sponsors-ufr">
          <li><a href="//www.dokuwiki.org/dokuwiki" id="dokuwiki">Propulsé par Dokuwiki</a></li>
        </ul>
      </div>

    </div>
    <!-- $Id$ -->
<?php /* /!\ remplacement par bin/update_indexer.sh éxécuté dans un cron toutes les minutes
	 <div class="no"><?php /* provide DokuWiki housekeeping, required in all templates */ /* tpl_indexerWebBug() ?></div>
       */ ?>

    <!-- Piwik -->
		<script type="text/javascript">
		  var _paq = _paq || [];
		  _paq.push(["setDomains", ["*.doc.ubuntu-fr.org","*.doc.edubuntu-fr.org","*.doc.lubuntu-fr.org","*.doc.xubuntu-fr.org","*.doc.edubuntu-fr.org","*.doc.lubuntu-fr.org","*.doc.ubuntu-fr.org","*.doc.xubuntu-fr.org"]]);
		  _paq.push(['trackPageView']);
		  _paq.push(['enableLinkTracking']);
		  (function() {
		    var u="//piwik.ubuntu-fr.org/";
		    _paq.push(['setTrackerUrl', u+'piwik.php']);
		    _paq.push(['setSiteId', 3]);
		    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
		    g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
		  })();
		</script>
		<noscript><p><img src="//piwik.ubuntu-fr.org/piwik.php?idsite=3" style="border:0;" alt="" /></p></noscript>
		<!-- End Piwik Code -->


  </body>
</html>
