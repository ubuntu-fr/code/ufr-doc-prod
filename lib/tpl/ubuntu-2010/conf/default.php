<?php
/**
 * Multi theme configuration, switch on domain name change
 *
 */

$conf['static_host'] = "//www-static.ubuntu-fr.org";
$conf['subdomain'] = "doc.";
$conf['domain']['ubuntu']   = "ubuntu-fr.org";
$conf['name']['ubuntu']     = "Ubuntu";
$conf['comp']['ubuntu']     = "d'";
$conf['indexed']['ubuntu']  = 1;

$conf['domain']['kubuntu']  = "kubuntu-fr.org";
$conf['name']['kubuntu']    = "Kubuntu";
$conf['comp']['kubuntu']    = "de ";
$conf['indexed']['kubuntu'] = 0;

$conf['domain']['xubuntu']  = "xubuntu-fr.org";
$conf['name']['xubuntu']    = "Xubuntu";
$conf['comp']['xubuntu']    = "de ";
$conf['indexed']['xubuntu'] = 0;

$conf['domain']['edubuntu'] = "edubuntu-fr.org";
$conf['name']['edubuntu']   = "Edubuntu";
$conf['comp']['edubuntu']   = "d'";
$conf['indexed']['edubuntu']= 0;


