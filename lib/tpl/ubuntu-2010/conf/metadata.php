<?php
/**
 * Multi theme configuration, switch on domain name change
 *
 */
$meta['_template'] = array('fieldset');
$meta['static_host'] = array('string');
$meta['subdomain'] = array('string');
$meta['domain']['ubuntu']   = array('string');
$meta['name']['ubuntu']     = array('string');
$meta['comp']['ubuntu']     = array('string');
$meta['indexed']['ubuntu']  = array('onoff');

$meta['domain']['kubuntu']  = array('string');
$meta['name']['kubuntu']    = array('string');
$meta['comp']['kubuntu']    = array('string');
$meta['indexed']['kubuntu'] = array('onoff');

$meta['domain']['xubuntu']  = array('string');
$meta['name']['xubuntu']    = array('string');
$meta['comp']['xubuntu']    = array('string');
$meta['indexed']['xubuntu'] = array('onoff');

$meta['domain']['edubuntu'] = array('string');
$meta['name']['edubuntu']   = array('string');
$meta['comp']['edubuntu']   = array('string');
$meta['indexed']['edubuntu']= array('onoff');


