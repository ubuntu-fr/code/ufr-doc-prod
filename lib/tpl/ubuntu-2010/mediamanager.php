<?php
    /**
     * Ubuntu-fr 2010 template
     *
     * @link   http://ubuntu-fr.org/
     * @author ubuntu-fr community
     */

    // must be run from within DokuWiki
    if (!defined('DOKU_INC')) die();

    // Theme selection
    $domain = $_SERVER['HTTP_HOST'];
    $subdomain = tpl_getConf('subdomain');
    $domain_names = tpl_getConf('domain');
    $names = tpl_getConf('name');
    $comps = tpl_getConf('comp');
    $indexes = tpl_getConf('indexed');
    
    $tplname = 'ubuntu'; //default theme
    foreach ($domain_names as $tpl => $url) {
        $compdomain = $subdomain . $url;
        if ($domain == $compdomain) {
            $tplname = $tpl;
        }
    }
    $domain_name = $domain_names[$tplname];
    $distrib_name = $names[$tplname];
    $distrib_comp = $comps[$tplname];
    $is_ubuntu = $indexes[$tplname];
    if ($conf['is_dev']) {
        $is_ubuntu = 0;
    }

    if (!defined('UFR_STATIC_TPL')) define('UFR_STATIC_TPL', tpl_getConf('static_host') .'/theme2010');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $conf['lang']?>" lang="<?php echo $conf['lang']?>" dir="ltr">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>
          <?php echo hsc($lang['mediaselect'])?>
          [<?php echo strip_tags($conf['title'])?>]
        </title>
        <?php tpl_metaheaders()?>
        <link rel="shortcut icon" href="<?=UFR_STATIC_TPL?>/images/commun/<?=strtolower($distrib_name)?>/icone.png" type="image/x-icon" />
        <link rel="apple-touch-icon" href="<?=UFR_STATIC_TPL?>/images/commun/<?=strtolower($distrib_name)?>/touch-ico.png" />
        <link rel="stylesheet" media="screen" type="text/css" title="Design <?=$distrib_name?>-fr" href="<?=UFR_STATIC_TPL?>/css/doc-media.css" />
    </head>

<body>
    <div id="media__manager">
        <div id="media__left">
            <h1 id="media__h1"><?php echo hsc($lang['mediaselect'])?>&nbsp;</h1>
            <div id="media__scroll">
                <?php /* keep the id! additional elements are inserted via JS here */?>
                <div id="media__opts"></div>
    
                <?php tpl_mediaTree() ?>
            </div>
        </div>

        <div id="media__right">
            <?php html_msgarea()?>
            <?php tpl_mediaContent() ?>
        </div>
    </div>
</body>
</html>
