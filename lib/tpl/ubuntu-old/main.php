<?php
    $domain_name = preg_replace('/(.*\.)??([^\.]*\.[^\.]*?)(:.*)?$/', '$2', $_SERVER['SERVER_NAME']);
    $is_ubuntu = 0;
    $is_kubuntu = 0;
    $is_edubuntu = 0;
    if ($domain_name == 'ubuntu-fr.org') {
        $distrib_name = 'Ubuntu';
        $distrib_comp = 'd\'';
        $is_ubuntu = 1;
    }
    elseif ($domain_name == 'xubuntu-fr.org') {
        $distrib_name = 'Xubuntu';
        $distrib_comp = 'de ';
        $is_kubuntu = 1;
    }
    elseif ($domain_name == 'kubuntu-fr.org') {
        $distrib_name = 'Kubuntu';
        $distrib_comp = 'de ';
        $is_kubuntu = 1;
    }
    elseif ($domain_name == 'edubuntu-fr.org') {
        $distrib_name = 'Edubuntu';
        $distrib_comp = 'd\'';
        $is_edubuntu = 1;
    }
    else 
    {
        $distrib_name = 'Ubuntu';
        $distrib_comp = 'd\'';
        $is_ubuntu = 0;
    }
    if ($conf['is_dev']) {
        $is_ubuntu = 0;
        $domain_name = "dev.".$domain_name;
    }
    if (!defined('UFR_STATIC')) define('UFR_STATIC', 'http://'. $conf['static_host'] .'/');
    if (!defined('UFR_STATIC_TPL')) define('UFR_STATIC_TPL', UFR_STATIC.'lib/tpl/');
    if (!defined('UFR_STATIC_IMG')) define('UFR_STATIC_IMG', UFR_STATIC.'images/');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?=$conf['lang']?>" lang="<?=$conf['lang']?>" dir="<?=$lang['direction']?>">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="Content-Script-Type" content="text/javascript"/>
        <meta http-equiv="Content-Style-Type" content="text/css"/>
        <meta http-equiv="Content-Language" content="fr"/>
      <? if (!$is_ubuntu): ?>
        <meta name="GOOGLEBOT" content="NOINDEX, NOFOLLOW" />
      <? endif ?>
        <?tpl_metaheaders()?>
        <title><?=$ID?> - <?=hsc($conf['title'])?></title>
        <link rel="shortcut icon" href="<?=DOKU_TPL?>images/favicon-<?=strtolower($distrib_name)?>.ico"/>
        <link rel="stylesheet" media="screen" type="text/css" title="Design Ubuntu" href="<?=UFR_STATIC_TPL?><?=$conf['template']?>/addedstyle-<?=strtolower($distrib_name)?>.css" />
        <link rel="stylesheet" media="print" type="text/css" title="Design Ubuntu" href="<?=UFR_STATIC_TPL?><?=$conf['template']?>/print.css" />
        <?php // FIXME use $INFO instead -- Ju.
            if ($_GET['do'] == 'shownumerotation') {
                   echo    "\n\t<style type=\"text/css\"> @import \"<?=UFR_STATIC?>/lib/styles/numberize.css\";</style\n";
            }
        ?>
	<script type="text/javascript">
	var menu_hidden = 0;
	var static_url = "<?=UFR_STATIC?>";
	</script>

        <script src="<?=UFR_STATIC?>/style/imports/menu.js" type="text/javascript"></script>
        <script src="<?=UFR_STATIC?>/style/imports/common.js" type="text/javascript"></script>

    </head>
    <body>
        <div id="page">
            <div id="top">
                <h1><a title="Accueil" href="http://www.<?=$domain_name?>"><?=strtolower($distrib_name)?>-fr</a></h1>
                <p>Communauté francophone des utilisateurs <?=$distrib_comp?><?=$distrib_name?></p>
                <form action="/" accept-charset="utf-8" class="search" id="search">
                    <input type="hidden" name="do" value="search" />
                    <input type="text" id="qsearch__in" accesskey="f" name="id" class="edit" title="[F]" />
                    <input type="submit" value="Recherche" class="button" title="Search" />
                </form>
            </div>
            <div id="navbar">
                <ul>
                    <li><a href="http://www.<?=$domain_name?>" id="navbar_www">Accueil</a></li>
                    <li id="active"><a href="/" id="navbar_doc">Documentation</a></li>
                    <li><a href="http://forum.<?=$domain_name?>" id="navbar_forum">Forum</a></li>
                    <li><a href="http://planet.<?=$domain_name?>" id="navbar_planet">Planet</a></li>
                </ul>
                <p>
                    Vous êtes ici : 
                    <a href="http://www.<?=$domain_name?>" title="Retour à l'accueil">Accueil</a>
                    &raquo; <a href="/" title="Documentation <?=strtolower($distrib_name)?>-fr">Documentation</a>
                </p>
            </div>

            <?php flush()?>

            <div id="navigation">
                <div id="navWiki">
                    <h2>Actions</h2>
                    <ul>
                        <li><?php tpl_button('index')?></li>
                        <li><?php tpl_button('edit')?></li>
                        <li><?php tpl_button('history')?></li>
                        <li><?php tpl_button('revert')?></li>
                        <li><?php tpl_button('recent')?></li>
                        <li><?php tpl_button('backlink')?></li>
                        <li><?php tpl_button('subscription')?></li>
                        <li><?php tpl_button('admin')?></li>
                        <li><?php tpl_button('login')?></li>
                        <li><?php tpl_userinfo()?></li>

                        <?php /* <li><?tpl_button('numerotation')?></li> */ ?>
                    </ul>
                </div>

                <div id="navDivers">
                    <h2>Divers</h2>
                    <ul>
                        <li><?php tpl_pagelink(":wiki:participer_wiki", "Participer à la documentation") ?></li>
                        <li><?php tpl_pagelink(":documentation_hors_ligne", "Documentation hors ligne") ?></li>
                        <li><a href="http://www.<?=$domain_name?>/telechargement" title="T&eacute;l&eacute;charger <?=$distrib_name?>">T&eacute;l&eacute;charger <?=$distrib_name?></a></li>
                        <li><a href="http://www.<?=$domain_name?>/partenaires" title="Ils nous soutiennent...">Partenaires</a></li>
                    </ul>
                </div>

            </div>
            <div id="main">
		<p id="hidemenu" style="margin:-18px 0 0 -18px;display:block;height:21px;width:21px;padding:0;"><a style="background:url(<?=UFR_STATIC?>/style/ubuntu/bouton-wiki-menu-hide.png) no-repeat;display:block;height:21px;width:21px;text-decoration:none" accesskey="z" title="Masquer le menu - AccessKey Z" href="javascript:switchMenuVisible()"><span>&nbsp;</span></a></p>
                <div id="pageinfo"><?php html_msgarea()?></div>
                <h2 id="pagetitle">[[<?tpl_link(wl($ID,''),$ID)?>]]</h2>
              <?if($conf['breadcrumbs']){?><p><?tpl_breadcrumbs()?></p><?}?>
                <?tpl_content()?>
                <br style="clear:both;" />
                <div id="pageinfo">
                    <!-- <?tpl_userinfo()?> | <?php echo $INFO['userinfo']['mail']. ' | '. $INFO['userinfo']['gprs']['0'] ?>-->
                    <?tpl_pageinfo()?>
                    <br />Le contenu de ce wiki est sous licence : <a href="http://creativecommons.org/licenses/by-sa/3.0/deed.fr" rel="license" target="_blank">CC BY-SA v3.0</a>
                </div> 
            </div>
            <p id="footer">
                <a id="noris" href="http://www.noris.net"><img src="<?=UFR_STATIC_IMG?>noris_logo.gif" style="margin-left:4px" alt="Hébergé par Noris Network"/></a>
                <?php flush()?>
                <a href="http://www.dokuwiki.org/dokuwiki"><img src="<?=UFR_STATIC_IMG?>dokuwiki_logo.png" alt="Propulsé par Dokuwiki" /></a>
            </p>
        </div>
        <!-- $Id$ -->
        <div class="no"><?php /* provide DokuWiki housekeeping, required in all templates */ tpl_indexerWebBug()?></div>
    </body>
</html>
