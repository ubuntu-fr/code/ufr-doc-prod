<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php
    $domain_name = preg_replace('/(.*\.)??([^\.]*\.[^\.]*?)(:.*)?$/', '$2', $_SERVER['SERVER_NAME']);
    $is_ubuntu = 0;
    $is_kubuntu = 0;
    $is_edubuntu = 0;
    if ($domain_name == 'ubuntu-fr.org') {
        $distrib_name = 'Ubuntu';
        $distrib_comp = 'd\'';
        $is_ubuntu = 1;
    }
    elseif ($domain_name == 'xubuntu-fr.org') {
        $distrib_name = 'Xubuntu';
        $distrib_comp = 'de ';
        $is_kubuntu = 1;
    }
    elseif ($domain_name == 'kubuntu-fr.org') {
        $distrib_name = 'Kubuntu';
        $distrib_comp = 'de ';
        $is_kubuntu = 1;
    }
    elseif ($domain_name == 'edubuntu-fr.org') {
        $distrib_name = 'Edubuntu';
        $distrib_comp = 'd\'';
        $is_edubuntu = 1;
    }
    else 
    {
        $distrib_name = 'Ubuntu';
        $distrib_comp = 'd\'';
        $is_ubuntu = 0;
    }
    if (!defined('UFR_STATIC')) define('UFR_STATIC', 'http://'. $conf['static_host'] .'/');
    if (!defined('UFR_STATIC_TPL')) define('UFR_STATIC_TPL', UFR_STATIC.'lib/tpl/');
    if (!defined('UFR_STATIC_IMG')) define('UFR_STATIC_IMG', UFR_STATIC.'images/');
?>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $conf['lang']?>" lang="<?php echo $conf['lang']?>" dir="ltr">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>
          <?php echo hsc($lang['mediaselect'])?>
          [<?php echo strip_tags($conf['title'])?>]
        </title>
        <?php tpl_metaheaders()?>
        <link rel="shortcut icon" href="<?=DOKU_TPL?>images/favicon-<?=strtolower($distrib_name)?>.ico"/>
        <link rel="stylesheet" media="screen" type="text/css" title="Design Ubuntu" href="<?=DOKU_TPL?>media.css" />
    </head>

<body>
    <div id="media__manager">
        <div id="media__left">
            <?php /* keep the id! additional elements are inserted via JS here */?>
            <div id="media__opts"></div>

            <?php tpl_mediaTree() ?>
        </div>

        <div id="media__right">
            <?php html_msgarea()?>
            <h1><?php echo hsc($lang['mediaselect'])?></h1>
            <?php tpl_mediaContent() ?>
        </div>
    </div>
</body>
</html>
