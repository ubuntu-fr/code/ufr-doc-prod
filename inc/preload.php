<?php
// Use private cache for authentified user to prevent proxy to cache the session.
// Pages shown by anonymous user will be cached
$offset = 300; // default validity time for proxies
// header('Vary: Cookie'); // Damn doku it puts cookie for everyone 
// with local info, ie. can't use Vary: Cookie here
if(!empty($_SERVER['REMOTE_USER'])){
    header('Cache-Control: private, no-store, max-age=0');
    header('Pragma: private');
}
else {
    header('Cache-Control: public, must-revalidate, max-age='.$offset);
    header('Pragma: public');
}

