#!/bin/bash 
#
# This script parses the dokuwiki changes file to call the indexer only on needed pages
# Maxence Dunnewind <maxence@dunnewind.net>
#

#Config part 

# Path to the root of dokuwiki
DOKU_PATH="/srv/www/doc.ubuntu-fr.org/htdocs/"

# The script will look for any changes with a timestamp > now() - $DELTA seconds
# You should set it a bit higher than the interval between 2 runs of the script to be sure you don't miss any page
DELTA=120 #Default : 90s, the script is called every 60s

#File use to ensure the script is not already running
LOCK=/tmp/indexer-lock

#base url to use, <pagename>&<modiftimestamp> will be added
BASEURL="http://127.0.0.1/lib/exe/indexer.php?id="

#debug, log all wget outputs
DEBUG=

###                  ###
## Do not change anything after this line ##
###                  ###
lockfile-check -l $LOCK && exit 0
lockfile-create -l $LOCK
trap "{ lockfile-remove -l $LOCK ; exit 255; }" EXIT

REFTIME=$(date +%s -d "now - $DELTA seconds")
tac ${DOKU_PATH}/data/meta/_dokuwiki.changes|while read line;do 
    time=$(echo $line|awk '{ print $1 }');
    page=$(echo $line|awk '{ print $4 }'|sed 's/:/\//g');
    if [ $time -gt $REFTIME ];then
        logger "Call indexer for page ${page} with timestamp ${time}"
        if [ $DEBUG ];then
            logs=$(wget -nv -O- "${BASEURL}${page}&${time}" 2>&1)
            logger "$logs"
        else
            wget -q -O/dev/null "${BASEURL}${page}&${time}" 
        fi
    else
        break
    fi
done

lockfile-remove -l $LOCK
trap - EXIT
exit 0
